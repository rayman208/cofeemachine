package com.company;

public class CoffeMachine
{
    private ICoffeeMakebale makebale;

    public CoffeMachine(ICoffeeMakebale makebale)
    {
        this.makebale = makebale;
    }

    public void setMakebale(ICoffeeMakebale makebale) {
        this.makebale = makebale;
    }

    public void DoCoffee()
    {
        this.makebale.MakeCoffee();
    }
}
