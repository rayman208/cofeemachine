package com.company;

public class Main {

    public static void main(String[] args) {
        CoffeMachine machine = new CoffeMachine(new LatteCoffee());
        machine.DoCoffee();

        machine.setMakebale(new CapuchinoCoffee());
        machine.DoCoffee();

        machine.setMakebale(new SOmeCoffee());
        machine.DoCoffee();

    }
}
